
all: main

main:
	cd source && $(MAKE)

doc:
	cd source && $(MAKE) doc

tests:
	cd source && $(MAKE) tests

clean:
	cd source && $(MAKE) clean

re: clean all
