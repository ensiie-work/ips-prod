# IPS-PROD

2ème projet d'IPS au S3 à l'ENSIIE

## Objectif

Calculer et afficher la densité locale d'un système nucléaire, définie ici : https://dubrayn.github.io/IPS-PROD/project.html#2

## Dépendances

* librairie de parseur json en c++ : https://github.com/nlohmann/json
  > Télécharger l'archive puis la décompresser dans un dossier `json-develop/` dans le dossier `source/`
* Matplotlib, librairie python pour l'affichage de plots : https://matplotlib.org/users/installing.html

## Compilation

* `make doc` génère la documentation au format html dans le dossier `doc/`
* `make` génère l'exécutable principal `oscillateur`. Les paramètres d'entrées sont à modifier dans le fichier `source/inputs.json`.
* `make tests` génère les exécutables de tests `testPoly` et `testBasis`.

L'exécutable principal se lance **depuis le dossier `source/`** avec :
```
./oscillateur [inputs.json]
```
Les données générées par l'exécutable sont disponibles dans `source/data.csv`.

## Affichage

L'affichage de la solution s'obtient avec la commande suivante :
```
python3 affichage.py
```