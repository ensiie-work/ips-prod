#include <armadillo>
#include <nlohmann/json.hpp>
#include "poly.h"
#include "basis.h"

using json = nlohmann::json;

int main(int ac, char **av) {
    int m_a, n_a, n_z_a;
    int m_b, n_b, n_z_b;

    double br, bz, Q;
    int N;

    arma::vec z, r;
    std::ifstream file;
    json params;

    arma::wall_clock timer;

    /* récupération des paramètres utilisateurs,
     * soit par le fichier en argument
     * soit par le fichier inputs.json par défaut */
    if (ac == 2) {
        file = std::ifstream(av[1]);
    }
    else {
        file = std::ifstream("inputs.json");
    }

    /* lecture des paramètres d'entrée via le fichier json */
    file >> params;
    br = params["br"];
    bz = params["bz"];
    Q = params["Q"];
    N = (int)params["N"];
    z = arma::linspace(params["zStart"], params["zEnd"], (int)params["nb_z"]);
    r = arma::linspace(params["rStart"], params["rEnd"], (int)params["nb_r"]);

    /* lecture de la matrice rho nécessaire au calcul de la solution */
    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    arma::cube rho_calc;
    
    // z = arma::linspace(-10.0, 10.0, 100);
    // r = {3.1, 2.3, 1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
    arma::mat res = arma::zeros(z.size(),r.size());
    arma::mat res2 = arma::zeros(z.size(),r.size());

    /* initialisation de la base */
    Basis basis(br, bz, N, Q);
   
    /* conversion des nombres quantiques en numero de colonne de la matrice rho */
    rho_calc.set_size(basis.mMax, basis.nMax(0), basis.n_zMax(0,0));
    uint i = 0;
    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
      /* on stocke le numero de colonne de rho correspondant aux nombres quantiques m, n, n_z
       * dans rho_calc(m, n, n_z) */
      rho_calc(m,n,n_z) = i;
      i++;
    }

    timer.tic();

    /* calcul de la solution en 2D */
    for (m_a = 0 ; m_a < basis.mMax ; m_a++)
    for (n_a = 0 ; n_a < basis.nMax(m_a) ; n_a++)
    for (n_z_a = 0 ; n_z_a < basis.n_zMax(m_a, n_a) ; n_z_a++)
    {
        arma::mat psi_a = basis.basisFunc(m_a, n_a, n_z_a, z, r);
        for (n_b = 0 ; n_b < basis.nMax(m_a) ; n_b++)
        for (n_z_b = 0 ; n_z_b < basis.n_zMax(m_a, n_b) ; n_z_b++)
        {
            arma::mat psi_b = basis.basisFunc(m_a, n_b, n_z_b, z, r);
            res2 += psi_b * rho(rho_calc(m_a, n_a, n_z_a), rho_calc(m_a, n_b, n_z_b));
        }
        res += psi_a % res2;
        res2 = arma::zeros(z.size(),r.size());
    }

    /* Temps de calcul de la solution*/
    double time = timer.toc();
    std::cout << "number of seconds: " << time << std::endl;

    /* pas nécessaire, simplement pour que les z se retrouvent en ligne et non en colonne. TODO A faire dans le .py */
//    res = res.t();

    /* on sauvegarde les données dans in fichier csv */
    res.save("datas.csv", arma::csv_ascii);
    return 0;
}
