#include "poly.h"

/**
 * \file poly.cpp
 * \brief Fonctions de calcul et d'accès aux polynômes d'Hermite et de Laguerre généralisés
 * \author Antoine Boudinelle 
 */

/**
 * \brief Calcule les polynômes d'Hermite d'indice 0 à n évalué aux valeurs de zVals.
 * \param n : correspond à l'indice maximum des polynômes d'Hermite calculés (doit donc être > 0).
 * \param zVals : correspond aux valeurs en lesquelles les polynômes sont évalués.
 * \return Stock le résultat dans l'attribut polyHermite.
 * \attention n doit être strictement positif.
 */
void Poly::calcHermite(int n, const arma::vec & zVals) {
    int nb_z = zVals.size();
    arma::mat res;
    arma::vec res_aux(n);
    int i,j;

    res.zeros(n,nb_z);

    for(j = 0 ; j < nb_z ; j++) {
        res_aux(0) = 1;
        res_aux(1) = 2.0 * zVals(j);
        for(i = 2 ; i < n ; i++){
            res_aux(i) = ((2.0 * zVals(j) * res_aux(i-1)) - (2.0 * (double)(i-1) * res_aux(i-2)));
        }
        res.col(j) = res_aux;
    }
    this->polyHermite = res;
}

/**
 * \brief Permet d'accèder au polynôme d'Hermite d'indice n précédement calculé.
 * \param n : correspond à l'indice du polynôme d'Hermite.
 * \return Renvoie le polynôme d'Hermite d'indice n.
 * \attention Il faut avoir fait au préalable un appel de calcHermite(N) avec n<=N.
 */
arma::vec Poly::hermite(int n) {
    return this->polyHermite.row(n).t();
}

/**
 * \brief Calcul les polynômes de laguerre d'indice {0:m}x{0:n} évalué aux valeurs de zVals.
 * \param m : correspond au premier indice maximum des polynômes de Laguerre calculés (doit donc être > 0).
 * \param n : correspond au second indice maximum des polynômes de Laguerre calculés (doit donc être > 0).
 * \parma zVals : correspond aux valeurs en lesquelles les polynômes sont évalués.
 * \return Stock le résultat dans l'attribut polyLaguerre.
 * \attention n et m doivent être strictement positif.
 */
void Poly::calcLaguerre(const int m, const int n, const arma::vec & rVals) {
    int nb_z = rVals.size();
    arma::cube res;
    int i,j,k;

    res.ones(nb_z,m,n);

    for(k = 0;k < nb_z;k++) {
        for(i = 0;i < m;i++) {
            res(k,i,1) = 1 + i - rVals(k);
            for(j = 2;j < n;j++) {
                res(k,i,j) = (2.0 + ((double)i - 1.0 - rVals(k)) / (double)j) * res(k,i,j-1) - (1.0 + (i - 1.0) / (double)j) * res(k,i,j-2);
            }
        }
    }
    polyLaguerre = res;
}

/**
 * \brief Permet d'accèder au polynôme de Laguerre d'indice m,n précédement calculé.
 * \param m : correspond au premier indice du polynôme de Laguerre.
 * \param n : correspond au second indice du polynôme de Laguerre.
 * \return Renvoie le polynôme de Laguerre d'indice m,n.
 * \attention Il faut avoir fait au préalable un appel de calcLaguerre(M,N) avec m<=M et n<=N. 
 */
arma::vec Poly::laguerre(int m, int n) {
    return polyLaguerre.slice(n).col(m);
}

