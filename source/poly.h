#ifndef POLY_H
#define POLY_H

#include <armadillo>

class Poly {

    //Attributs
    private:
    /**
     * \brief Polynômes de l'Hermite d'indice 0 à n évalué aux valeurs de zVals (n et zvals sont les arguments du dernier appel de calcHermite).
     * Contient le dernier résultat de la méthode calcHermite.
     */
         arma::mat polyHermite;
    
    /**
     * \brief Polynômes de Laguerre d'indice dans {0:m}x{o:n} évalué aux valeurs de zVals (m, n et zvals sont les arguments du dernier appel de calcLaguerre).
     * Contient le dernier résultat de la méthode calcLaguerre.
     */
        arma::cube polyLaguerre;

    //Méthodes
    public:
        void calcHermite(int n, const arma::vec & zVals);
        void calcLaguerre(int m, int n, const arma::vec & rVals);
        arma::vec hermite(int n);
        arma::vec laguerre(int m, int n);
};

#endif
