# Temps pour les différents algorithmes de calcul de la solution 2D

## Paramètres à utiliser
```
{
    "br": 1.935801664793151,
    "bz": 2.829683956491218,
    "N": 14,
    "Q": 1.3,
    "zStart":-10,
    "zEnd":10,
    "nb_z":50,
    "rStart":-10,
    "rEnd":10,
    "nb_r":50
}
```

## Algorithme naïf : Durée 10.6263s
```
    for (m_a = 0 ; m_a < basis.mMax ; m_a++)
    for (n_a = 0 ; n_a < basis.nMax(m_a) ; n_a++)
    for (n_z_a = 0 ; n_z_a < basis.n_zMax(m_a, n_a) ; n_z_a++)
    for (m_b = 0 ; m_b < basis.mMax ; m_b++)
    for (n_b = 0 ; n_b < basis.nMax(m_b) ; n_b++)
    for (n_z_b = 0 ; n_z_b < basis.n_zMax(m_b, n_b) ; n_z_b++)
    {
        arma::mat psi_a = basis.basisFunc(m_a, n_a, n_z_a, z, r);
        arma::mat psi_b = basis.basisFunc(m_b, n_b, n_z_b, z, r);
        res += psi_a % psi_b * rho(rho_calc(m_a, n_a, n_z_a),rho_calc(m_b, n_b, n_z_b));
    }
```
## En utilisant le fait que Rho est m-diagonal : Durée 1.23944s
```
    for (m_a = 0 ; m_a < basis.mMax ; m_a++)
    for (n_a = 0 ; n_a < basis.nMax(m_a) ; n_a++)
    for (n_z_a = 0 ; n_z_a < basis.n_zMax(m_a, n_a) ; n_z_a++)
    for (m_b = 0 ; m_b < basis.mMax ; m_b++)
    for (n_b = 0 ; n_b < basis.nMax(m_b) ; n_b++)
    for (n_z_b = 0 ; n_z_b < basis.n_zMax(m_b, n_b) ; n_z_b++)
    {
        if (m_a != m_b) continue;
        arma::mat psi_a = basis.basisFunc(m_a, n_a, n_z_a, z, r);
        arma::mat psi_b = basis.basisFunc(m_b, n_b, n_z_b, z, r);
        res += psi_a % psi_b * rho(rho_calc(m_a, n_a, n_z_a),rho_calc(m_b, n_b, n_z_b));
    }
```
## Suppression d'une loop en utilisant un delta_ma_mb : Même durée

## Déplacement du calcul de psi_a le plus a gauche possible : Durée 0.809801s
```
    for (m_a = 0 ; m_a < basis.mMax ; m_a++)
    for (n_a = 0 ; n_a < basis.nMax(m_a) ; n_a++)
    for (n_z_a = 0 ; n_z_a < basis.n_zMax(m_a, n_a) ; n_z_a++)
    {
    arma::mat psi_a = basis.basisFunc(m_a, n_a, n_z_a, z, r);
    for (n_b = 0 ; n_b < basis.nMax(m_a) ; n_b++)
    for (n_z_b = 0 ; n_z_b < basis.n_zMax(m_a, n_b) ; n_z_b++)
    {
        arma::mat psi_b = basis.basisFunc(m_a, n_b, n_z_b, z, r);
        res += psi_a % psi_b * rho(rho_calc(m_a, n_a, n_z_a),rho_calc(m_a, n_b, n_z_b));
    }
    }
```

## Passage des tableaux par référence dans les arguments des fonctions : Durée 1.03 mais pas sur le même ordi (un peu plus rapide sinon)

```
    const arma::vec & rPart
```

## Compilation avec -O3 : Durée 0.15354s 

## Délai de la multiplication avec psi_a : Durée 0.1497s

```
    for (n_b = 0 ; n_b < basis.nMax(m_a) ; n_b++)
    for (n_z_b = 0 ; n_z_b < basis.n_zMax(m_a, n_b) ; n_z_b++)
    {
        arma::mat psi_b = basis.basisFunc(m_a, n_b, n_z_b, z, r);
        res2 += psi_b * rho(rho_calc(m_a, n_a, n_z_a), rho_calc(m_a, n_b, n_z_b));
    }
    res += psi_a % res2;
    res2 = arma::zeros(z.size(),r.size());
 ```
