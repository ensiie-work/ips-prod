#include "basis.h"

/**
 * \file basis.cpp
 * \brief code des fonctions de calcul et de troncature de la base
 * \author 
 */

Basis::Basis(const double br, const double bz, const int N, const double Q) 
{
	int i = 0, j=0;
	double tmp;

        this->br = br;
        this->bz = bz;
	tmp = nzmaxparam(N, Q, i);
	while (tmp >= 1)
	{
		i++;
		tmp = nzmaxparam(N, Q, i);
	}

        /* une incrémentation de trop pour i en sortie de boucle */
	mMax = i-1;

	nMax = arma::ivec(mMax);
	for (i=0 ; i<mMax ; i++) {
		nMax(i) = (mMax - i - 1)/2 + 1;
        }
	n_zMax.set_size(mMax, nMax(0));
	n_zMax.fill(0);
	for (i=0 ; i<mMax ; i++)
		for (j=0 ; j<nMax(i) ; j++)
			n_zMax(i,j) = nzmaxparam(N, Q, i+(2*j)+1);

}

/**
 * \brief constructeur par défaut qui appelle le constructeur valué avec les valeurs du cours
 */
Basis::Basis()
{
	const double br = 1.93580166479315;
	const double bz = 2.829683956491218;
	const int N = 14;
	const double Q = 1.3;

	Basis(br, bz, N, Q);
}

/**
 * \brief fonction de calcul pour la troncation de la base
 * \param N paramètre de la troncation
 * \param Q paramètre de la troncation
 * \param i point en lequel on évalue la fonction
 * \return calcule la valeur \f$ (N+2)Q^{\frac{2}{3}}+\frac{1}{2}-iQ \f$
 */
double Basis::nzmaxparam(const int N, const double Q, const int i)
{
    /* problème lors de l'écriture en forme de fraction de 2/3 ..? */
    return ((N+2)*pow(Q, 0.6666666666) + 0.5 - i*Q);
}

/**
 * \brief calcule la factorielle d'un entier
 * \param n l'entier en question
 * \return calcule \f$ n! \f$
 * \attention n doit être positif
 */
double fact(int n) {
    if (n < 0) return 0.0;
    return (n==0 || n==1) ? 1.0 : fact(n-1) * ((double)n);
}

/**
 * \brief fonction de calcul de la partie radiale de la solution
 * \param r valeurs radiales en lesquelles on évalue la fonction
 * \param m nombre quantique
 * \param n nombre quantique
 * \return calcule la valeur \f$ 1/br \sqrt{n}... \f$
 */
arma::vec Basis::rPart(const arma::vec & r, double m, double n) {
    int i, N;
    N = r.size();
    arma::vec res(N);
    arma::vec laguerre(N);

    Poly poly;

    // si m et n valent 0 ou 1, le cube de polynôme de Laguerre se génère mal
    if (m<2 || n<2)
        poly.calcLaguerre(m+2, n+2, r % r / pow(br, 2) );
    else
        poly.calcLaguerre(m+1, n+1, r % r / pow(br, 2) );
    laguerre = poly.laguerre(m, n);

    for (i=0 ; i<N ; i++) {
        res(i) = 1/(br*sqrt(arma::datum::pi)) * sqrt( fact(n)/fact(n+m) )
            * exp(-1 * pow(r(i),2) / (2*pow(br,2)) ) * pow(r(i)/br,m);
    }
    res = laguerre % res;
    return res;
}

/**
 * \brief fonction de calcul sur l'axe z de la solution
 * \param z valeurs sur l'axe z en lesquelles on évalue la fonction
 * \param nz nombre quantique
 * \return calcule la valeur \f$ 1/bz \f$
 */
arma::vec Basis::zPart(const arma::vec & z, double nz) {
    int i, N;
    N = z.size();
    arma::vec res(N);
    arma::vec hermite(N);

    Poly poly;

    // si nz vaut 1 ou 0, la matrice des polynômes se génère mal.
    if (nz < 2)
        poly.calcHermite(nz+2, z/bz);
    else
        poly.calcHermite(nz+1, z/bz);
    hermite = poly.hermite(nz);

    for (i=0 ; i<N ; i++) {
        res(i) = 1/sqrt(bz) * 1/sqrt( pow(2,nz)*sqrt(arma::datum::pi)* fact(nz) )
            * exp( -1 * pow(z(i),2) / (2*pow(bz,2)) );
    }
    res = hermite % res;
    return res;
}

/**
 * \brief fonction principale de calcul de la solution par multiplication des vecteurs zPart et rPart
 * \param m nombre quantique en lequel évaluer la solution
 * \param n nombre quantique en lequel évaluer la solution
 * \param n_z nombre quantique en lequel évaluer la solution
 * \param z vecteur des valeurs sur l'axe z
 * \param r vecteur des valeurs sur l'axe radial
 * \return retourne la matrice correspondant à zPart * rPart
 */
arma::mat Basis::basisFunc(int m, int n, int nz, const arma::vec & z, const arma::vec & r) {
    arma::mat res;
    arma::vec zval = zPart(z, nz);
    arma::rowvec rval = rPart(r, m, n).t();

    res = zval * rval;
    return res;
}
