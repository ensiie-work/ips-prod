import matplotlib.pyplot as plt
import numpy as np
import json
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
from matplotlib import cm

# 3D
#fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')

# lecture des valeurs calculées de la solution
f = open('datas.csv')

# lecture des paramètres d'entrées via le fichier json
with open('inputs.json', 'r') as inputs:
    params_str = inputs.read()

params = json.loads(params_str)

y = []
# ajout des valeurs de la solution dans la matrice y
for line in f.readlines():
    y.append([float(n) for n in line.split(',')])

# axe des valeurs de z et de r suivant les paramètres
x_z = np.linspace(params["zStart"],params["zEnd"],params["nb_z"])
x_r = np.linspace(params["rStart"],params["rEnd"],params["nb_r"])
r = np.array(y)

Z = np.array(y).T
#ny, nx = Z.shape
#x = np.linspace(0, 1, nx)
#y = np.linspace(0, 1, ny)
xv, yv = np.meshgrid(x_z, x_r)

# affichage 3D de la solution
# surface=ax.plot_surface(xv, yv , Z, cmap=cm.coolwarm)

# vue 2D de dessus
im = plt.imshow(r, cmap='hot', extent=[params["zStart"], params["zEnd"], params["rStart"], params["rEnd"]])
plt.colorbar(im, orientation='horizontal', label="Intensité")
plt.xlabel("Z values")
plt.ylabel("r values")
plt.title("Intensité de la densité en fonction de Z et r")

# affichage 2D de la solution souhaitée
#plt.plot(x_z,y[0])
plt.show()

f.close()
