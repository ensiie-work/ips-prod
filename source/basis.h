#ifndef BASIS_H
#define BASIS_H

#include <armadillo>
#include <cmath>

#include "poly.h"
/**
 * \file basis.h
 * \brief classe Basis contenant les troncatures de la base et les fonctions de calcul
 * \author Charles Anteunis
 */

class Basis
{
public:
	Basis();
	Basis(double, double, int, double);

	int mMax;
	arma::ivec nMax;
	arma::imat n_zMax;
        double br;
        double bz;

        arma::vec rPart(const arma::vec &, double, double);

        arma::vec zPart(const arma::vec &, double);

        arma::mat basisFunc(int, int, int, const arma::vec &, const arma::vec &);

private:
	double nzmaxparam(int, double, int);
};

#endif
